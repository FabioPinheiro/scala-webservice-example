name          := """Scala-WebService-Example"""
organization  := "org.bitbucket.fabiopinheiro"
version       := "0.0.3"
scalaVersion  := "2.11.8"
scalacOptions := Seq("-unchecked", "-feature", "-deprecation", "-encoding", "utf8")
//scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")



libraryDependencies ++= {
  val akkaV       = "2.4.7" // "2.4.3"
  val scalazV     = "7.3.0-M2"
  val scalaTestV  = "3.0.0-M15" // "2.2.6"
  Seq(
    "io.spray" %%  "spray-json" % "1.3.2",
    "org.mongodb.scala" %% "mongo-scala-driver" % "1.1.1",
    //akka-http-spray-json-experimental: library for marshalling Scala data types into JSON.
    "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaV
    //"org.scalaz" %% "scalaz-core" % scalazV,
    //akka-actor: Actor system that akka-http and akka-stream are based on.
    //"com.typesafe.akka" %% "akka-actor" % akkaV,
    //akka-stream: library implementing Reactive Streams
    //"com.typesafe.akka" %% "akka-stream" % akkaV,
    //"com.typesafe.akka" %% "akka-http-core" % akkaV,
    //akka-http-experiental: library for processing HTTP requests using akka-streams and actors.
    //"com.typesafe.akka" %% "akka-http-experimental" % akkaV,
    //akka-http-testkit: library that helps testing akka-http routing and responses.
    //"com.typesafe.akka" %% "akka-http-testkit" % akkaV % "test",
    //scalatest: standard Scala testing library.
    //"org.scalatest"     %% "scalatest" % scalaTestV % "test"
  )
}


lazy val root = project.in(file(".")).configs(IntegrationTest)

scalastyleConfig := baseDirectory.value / "scalastyle-config.xml"

//resolvers += Resolver.jcenterRepo //This is the Bintray JCenter repository at https://jcenter.bintray.com/
//Revolver.settings
//Defaults.itSettings //adds compilation, packaging, and testing actions and settings in the IntegrationTest configuration.
//enablePlugins(JavaAppPackaging)

/* for th REPL
initialCommands := """|import scalaz._
                      |import Scalaz._
                      |import akka.actor._
                      |import akka.pattern._
                      |import akka.util._
                      |import scala.concurrent._
                      |import scala.concurrent.duration._""".stripMargin*/


fork in run := false
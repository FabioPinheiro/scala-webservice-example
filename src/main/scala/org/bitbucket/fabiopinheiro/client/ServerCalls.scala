package org.bitbucket.fabiopinheiro.client

import java.io.IOException

import akka.actor.ActorSystem
import akka.event.{ Logging, LoggingAdapter }
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, HttpRequest, HttpResponse }
import akka.http.scaladsl.unmarshalling._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Sink, Source }
import org.bitbucket.fabiopinheiro._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait ClientFunction extends ServerCalls with NodeServerCalls with EdgeServerCalls

trait SystemSettings {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  implicit val settings = Settings(system)
  implicit val logger: LoggingAdapter = Logging(system, getClass)

  lazy val serverConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    Http().outgoingConnection(settings.Http.interface, settings.Http.port)

  def serverCallRequest(request: HttpRequest): Future[HttpResponse] =
    Source.single(request).via(serverConnectionFlow).runWith(Sink.head)

}

trait ServerCalls extends SystemSettings with JsonSupport {

  def errorHanding[T](response: HttpResponse): Future[T] = {
    response.status match {
      case BadRequest => Future.failed(new IOException(s"This is a BadRequest(${response.status})"))
      case _ => {
        val msg = s"Request failed with status code ${response.status}"
        logger.error(msg)
        Future.failed(new IOException(msg))
      }
    }
  }
}

trait NodeServerCalls extends ServerCalls {
  val nodeEndPoint = "/node"

  def nodeCreate(nodeContent: Content): Future[String] = {
    val entity = HttpEntity(ContentTypes.`application/json`, contentFormat.write(nodeContent).compactPrint) //FIXME HOTFIX
    serverCallRequest(RequestBuilding.Post(s"$nodeEndPoint").withEntity(entity)).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[String]
        case _ => errorHanding(response)
      }
    }
  }

  def nodeRead(id: Long): Future[Node] = {
    serverCallRequest(RequestBuilding.Get(s"$nodeEndPoint?id=$id")).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[Node]
        case _ => errorHanding(response)
      }
    }
  }

  def nodeUpdate(id: Long, nodeContent: Content): Future[String] = {
    val entity = HttpEntity(ContentTypes.`application/json`, contentFormat.write(nodeContent).compactPrint)
    serverCallRequest(RequestBuilding.Put(s"$nodeEndPoint?id=$id").withEntity(entity)).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[String]
        case _ => errorHanding(response)
      }
    }
  }

  def nodeDelete(id: Long): Future[String] = {
    serverCallRequest(RequestBuilding.Delete(s"$nodeEndPoint?id=$id")).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[String]
        case _ => errorHanding(response)
      }
    }
  }
}

trait EdgeServerCalls extends ServerCalls {
  val edgeEndPoint = "/edge"

  def edgeCreate(edge: Edge): Future[String] = {
    val entity = HttpEntity(ContentTypes.`application/json`, edgeFormat.write(edge).compactPrint) //HOTFIX
    serverCallRequest(RequestBuilding.Post(s"$edgeEndPoint").withEntity(entity)).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[String]
        case _ => errorHanding(response)
      }
    }
  }

  def edgeRead(from: Option[Long], to: Option[Long]): Future[String] = { //FIXME Does not return a String TODO in server
    val path: String = s"$edgeEndPoint" + Seq(
      from match { case Some(id) => s"idFrom=$id"; case _ => },
      to match { case Some(id) => s"idTo=$id"; case _ => }
    ).mkString("?", "&", "") //build the call with parameters.
    serverCallRequest(RequestBuilding.Get(path)).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[String]
        case _ => errorHanding(response)
      }
    }
  }
}

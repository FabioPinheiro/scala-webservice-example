package org.bitbucket.fabiopinheiro

import org.bitbucket.fabiopinheiro.service.{ DB, Level }

import scala.collection.mutable._

/**
 * Created by fabio on 06-07-2016.
 */
trait ShortestPathFunctions {
  case class NodeID(id: Long)

  /**
   * @see [[https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm "Wikipedia"]]
   * @note Dijkstra's algorithm
   * @note Implementation base on [[https://github.com/yin/scala-dijkstra/blob/master/src/Dijkstra.scala]]
   */
  def dijkstra(start: NodeID, target: NodeID): (Map[NodeID, Int], Map[NodeID, NodeID]) = {
    val stopCondition: StopCondition = (S, D, P) => !S.contains(target)
    var queue: Set[NodeID] = new HashSet()
    var settled: Set[NodeID] = new HashSet()
    var distance: Map[NodeID, Int] = new HashMap()
    var path: Map[NodeID, NodeID] = new HashMap()
    queue += start
    distance(start) = 0

    while (!queue.isEmpty && stopCondition(settled, distance, path)) {
      val u = extractMinimum(queue, distance)
      settled += u
      relaxNeighbors(u, queue, settled, distance, path, Direction.Ahead)
    }

    return (distance, path)
  }

  /**
   * @author= Fabio Pinheiro
   * @note Dijkstra's algorithm derivation double source
   */
  def dijkstraDS(s1: NodeID, s2: NodeID): (Map[NodeID, Int], Map[NodeID, NodeID]) = {
    val s1stopCondition: StopCondition = (S, D, P) => !S.contains(s2)
    val s2stopCondition: StopCondition = (S, D, P) => !S.contains(s1)

    var s1queue: Set[NodeID] = new HashSet()
    var s1settled: Set[NodeID] = new HashSet()
    var s1distance: Map[NodeID, Int] = new HashMap()

    var s2queue: Set[NodeID] = new HashSet()
    var s2settled: Set[NodeID] = new HashSet()
    var s2distance: Map[NodeID, Int] = new HashMap()

    var path: Map[NodeID, NodeID] = new HashMap()

    s1queue += s1
    s1distance(s1) = 0
    s2queue += s2
    s2distance(s2) = 0

    //println(s"AQUI!! ${!queue.isEmpty} && ${stopCondition(settled, distance, path)}")
    while (!(s1queue.isEmpty || s1queue.isEmpty) &&
      s1stopCondition(s1settled, s1distance, path) &&
      s2stopCondition(s2settled, s2distance, path) &&
      true == false //TODO missing the test to compare the intersection (very important!!!)
      ) {

      val s1u = extractMinimum(s1queue, s1distance)
      s1settled += s1u
      relaxNeighbors(s1u, s1queue, s1settled, s1distance, path, Direction.Ahead)

      val s2u = extractMinimum(s1queue, s2distance)
      s2settled += s2u
      relaxNeighbors(s2u, s2queue, s2settled, s2distance, path, Direction.Backward)
      //println(s"NEXT!! ${!queue.isEmpty} && ${stopCondition(settled, distance, path)}")
    }

    return (null, path) // FIXME distance...... I should do the cost calculation here
  }

  type StopCondition = (Set[NodeID], Map[NodeID, Int], Map[NodeID, NodeID]) => Boolean

  protected def extractMinimum[T](Q: Set[T], D: Map[T, Int]): T = {
    var u = Q.head //first
    Q.foreach((node) => if (D(u) > D(node)) u = node)
    Q -= u
    return u;
  }

  object Direction extends Enumeration {
    val Ahead, Backward, Any = Value
  }

  protected def relaxNeighbors(u: NodeID, Q: Set[NodeID], S: Set[NodeID],
    D: Map[NodeID, Int], P: Map[NodeID, NodeID], direction: Direction.Value): Unit = {
    val aux = direction match { //FIXME direct connection to database
      case Direction.Ahead => DB.findNodesFrom(u.id)
      case Direction.Backward => DB.findNodesTo(u.id)
      case Direction.Any => DB.findNodesFrom(u.id) ++ DB.findNodesTo(u.id)
    }
    for (v <- aux.map(n => NodeID(n.id))) {
      if (!S.contains(v)) {
        if (!D.contains(v) || D(v) > D(u) + 1) { //cost = 1
          D(v) = D(u) + 1 // cost = 1
          P(v) = u
          Q += v
        }
      }
    }
  }

  def printSolution(start: NodeID, target: NodeID, distance: Map[NodeID, Int], path: Map[NodeID, NodeID]): Unit = {
    var shortest = List(target)
    if (path.exists(e => e._2.id == target.id)) {
      while (shortest.head != start) { shortest ::= path(shortest.head) }
      println(s"Shortest-path: cost=${distance(target)}; path=(${shortest.map(n => n.id).mkString(" -> ")})")
    } else { println(s"Shortest-path: Can't go there. There is no solution from=${start.id} to=${target.id}") }
  }
}

object ShortestPath extends App with ShortestPathFunctions {

  def inicializarDB: Unit = {
    DB.nextNodeID to DB.nextNodeID + 2 foreach { _ => DB.addNode(None, Level.Cache) } //1 to 3
    DB.nextNodeID to DB.nextNodeID + 6 foreach { _ => DB.addNode(None) } //4 to 10
    DB.addEdge(4, 1)
    DB.addEdge(5, 2)
    DB.addEdge(3, 5)
    DB.addEdge(5, 4)
    DB.addEdge(6, 7)
    DB.addEdge(3, 6)
    DB.addEdge(7, 8)

    DB.nextNodeID to DB.nextNodeID + 2 foreach { _ => DB.addNode(None, Level.Cache) } //11 to 13
    DB.addEdge(8, 11)
    DB.addEdge(11, 12)

    DB.addEdge(9, 13)
    DB.addEdge(9, 1)
    DB.addEdge(13, 9)
    // DB.addEdge(11, 13) //BUG FIXME updateCentroidEdges is just check a level depth! But i have a solution with a process of O(x) distance between centroids
  }

  inicializarDB

  println("### DB.edges ###")
  DB.edges foreach { e => println(e) }
  println("### DB.nodes ###")
  DB.nodes.foreach(n => println(n))
  println("### DB.centroidEdge ###")
  DB.centroidEdges.foreach(e => println(e))

  println("### ShortestPathFunctions ###")
  val (start, target) = (NodeID(6), NodeID(11))
  val (distance, path) = dijkstra(start, target)
  printSolution(start, target, distance, path)
  //val (null, path2) = dijkstraDS(start, target)
  //printSolution(start, target, null, path2)

}

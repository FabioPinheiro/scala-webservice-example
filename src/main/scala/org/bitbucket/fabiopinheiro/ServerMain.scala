package org.bitbucket.fabiopinheiro

import akka.actor._
import akka.event.Logging
import akka.event.Logging.InfoLevel
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.directives.DebuggingDirectives._
import akka.stream.ActorMaterializer
import com.typesafe.config._
import org.bitbucket.fabiopinheiro.service._

class SettingsImpl(config: Config) extends Extension {
  object Http {
    lazy val interface = config.getString("http.interface")
    lazy val port = config.getInt("http.port")
  }
}

object Settings extends ExtensionId[SettingsImpl] with ExtensionIdProvider {
  override def lookup = Settings
  override def createExtension(system: ExtendedActorSystem) = new SettingsImpl(system.settings.config)
}

object ServerMain extends App with GraphRoutes {
  implicit val system = ActorSystem(name = "ServerMain")
  implicit val materializer = ActorMaterializer()
  implicit val executor = system.dispatcher

  val settings = Settings(system)

  val logger = Logging(system, getClass)

  val routes = logRequestResult("", InfoLevel)(graphRoutes)

  Http().bindAndHandle(routes, settings.Http.interface, settings.Http.port) map { binding =>
    logger.info(s"Server started on port {}", binding.localAddress.getPort)
  } recoverWith { case _ => system.terminate() }
}
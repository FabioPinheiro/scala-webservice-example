package org.bitbucket.fabiopinheiro.service
//http://doc.akka.io/docs/akka-stream-and-http-experimental/1.0/scala/http/common/marshalling.html
import java.io.IOException

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import org.bitbucket.fabiopinheiro._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._

trait GraphRoutes extends JsonSupport {
  var db = DB

  def fetchNode(id: Long): Future[Either[String, Node]] = { //TESTING
    db.getNode(id) match {
      case Some(node) => Future.successful(Right(node));
      case None => Future.successful(Left("Failed: id not found."));
      case _ => val error = s"WHY? "; Future.failed(new IOException(error)) //logger.error(error)
    }
  }

  val graphRoutes: Route =
    // ~ Composing Routes http://doc.akka.io/docs/akka/2.4.7/scala/http/routing-dsl/routes.html#id1
    // ~ http://doc.akka.io/docs/akka/2.4.7/scala/http/routing-dsl/path-matchers.html#pathmatcher-dsl
    pathPrefix("node") {
      (post & entity(as[Content])) { c =>
        complete(s"Add node with id ${db.addNode(Some(c)).id}")
      } ~
        (get & parameters('id.as[Long])) { id =>
          complete {
            fetchNode(id).map[ToResponseMarshallable] {
              case Right(node) => node
              case Left(errorMessage) => BadRequest -> errorMessage
            }
          }
        } ~
        (put & parameters('id.as[Long]) & entity(as[Content])) { (id, c) =>
          db.updateNode(id, c) match {
            //http://www.restapitutorial.com/lessons/httpmethods.html
            case true => complete(s"Node $id updated with $c") // 200 (OK)
            case false => complete(s"Node $id not found") //FIXME  return 204 (No Content). 404 (Not Found)
          }
        } ~
        (delete & parameters('id.as[Long])) { id =>
          db.removeNode(id)
          complete(s"No more node $id if there was any")
        }
    } ~
      pathPrefix("edge") {
        (post & entity(as[Edge])) { e =>
          complete(s"Add edge $e") //TODO
        } ~
          (get & parameters('idFrom.as[Long].?, 'idTo.as[Long].?)) { (from, to) =>
            (from, to) match {
              case (Some(from), Some(to)) => complete(s"get edge idFrom=$from idTo=$to") //TODO (hum list with one edge?!)
              case (Some(from), None) => complete(s"get edges idFrom=$from") //TODO
              case (None, Some(to)) => complete(s"get edges idTo=$to") //TODO
              case (None, None) => complete(s"get edges all") //TODO
            }
          } ~
          //"update" DONE (not implemented)
          (delete & parameters('idFrom.as[Long], 'idTo.as[Long])) { (from, to) =>
            complete(s"Delete the edge from id $from to id $to") //TODO
          }
      }
}

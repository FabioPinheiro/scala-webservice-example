package org.bitbucket.fabiopinheiro.service

import akka.actor.ActorSystem
import akka.event.{ Logging, LoggingAdapter }

import scala.language.postfixOps

object MongoMain extends App {
  implicit val system: ActorSystem = ActorSystem(name = "MongoMain")
  val logger: LoggingAdapter = Logging(system, getClass)

  logger.info(s"##### $getClass #####" + getClass.getSimpleName)
  logger.info("##### Hello MongoDB #####")
  val DB = new MongoDB()(system = system)

  logger.info("# addNode #")
  val n = DB.addNode(None, Level.Base)
  logger.info(s"# node=${n.id} # $n")

  logger.info("# getNode #")
  val n2 = DB.getNode(n.id).get //1978183256L).get
  logger.info(s"# node=${n2.id} # $n2")

  logger.info("### close! ###")
  DB.close()
}

package org.bitbucket.fabiopinheiro.service

import akka.actor.ActorSystem
import akka.event.{ Logging, LoggingAdapter }
import com.mongodb.ServerAddress
import org.bitbucket.fabiopinheiro._
import org.mongodb.scala.connection.ClusterSettings
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.{ Completed, Document, MongoClient, MongoClientSettings, MongoCollection, MongoDatabase, Observable, Observer }
import spray.json._

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

trait MongoConnection {
  val logger: LoggingAdapter

  val clusterSettings: ClusterSettings = ClusterSettings.builder()
    .hosts(List(new ServerAddress("localhost")).asJava)
    .description("#Mongo Local Server#")
    .build()
  val settings: MongoClientSettings = MongoClientSettings.builder().clusterSettings(clusterSettings).build()
  val client: MongoClient = MongoClient(settings)
  val database: MongoDatabase = client.getDatabase("graph")

  def close(time: Long = 5): Unit = { Thread.sleep(time); client.close() }

  //TODO plz see https://github.com/mongodb/mongo-scala-driver/blob/master/examples/src/test/scala/tour/Helpers.scala
 /* val observerLogger = new Observer[Any] {
    override def onNext(result: Any): Unit = logger.info(s"# onNext # ${result}")
    override def onError(e: Throwable): Unit = logger.info(s"# onError # ${e.toString}")
    override def onComplete(): Unit = logger.info("### onComplete ###")
  }*/

  def initCollection(name: String): MongoCollection[Document] = {
    val observableListCollectionNames = database.listCollectionNames
    //FIXME observableListCollectionNames.subscribe(observerLogger)

    if (!Await.result(observableListCollectionNames.toFuture(), Duration.Inf).exists(s => s == name)) { //checks if the collection does not exists
      val observable = database.createCollection(name)
      //FIXME observable.subscribe(observerLogger)
      Await.result(observable.toFuture(), Duration.Inf)
    }
    database.getCollection(name)
  }

}

class MongoDB()(implicit val system: ActorSystem) extends MongoConnection with StorageInterface with JsonSupportMongo {
  val logger: LoggingAdapter = Logging(system, getClass)

  val nodesCollection: MongoCollection[Document] = initCollection("nodes")
  val edgesCollection: MongoCollection[Document] = initCollection("edges")
  val clusterEdgesCollection: MongoCollection[Document] = initCollection("clusterEdges")

  def toDocument(node: Node): Document = {
    val json = nodeFormat.write(node)
    Document(json.compactPrint)
  }

  def toDocument(edge: Edge): Document = Document(edgeFormat.write(edge).compactPrint)

  import org.bson.types.ObjectId

  override def addNode(content: Option[Content], nodeType: Level.Value): Node = {
    val _id = new ObjectId().hashCode().toLong
    val metaData = nodeType match {
      case Level.Base => MetaData(0, 0, 0, 0, isCentroid = false)
      case Level.Cache => MetaData(_id, 0, _id, 0, isCentroid = true)
    }
    val node = Node(_id, content, metaData)
    val doc = toDocument(node)
    logger.info(s"addNode: $doc")
    val observable: Observable[Completed] = nodesCollection.insertOne(doc)
    //FIXME observable.subscribe(observerLogger)
    Await.result(observable.toFuture(), Duration.Inf) //FIXME bolck!!
    node
  }

  override def getNode(id: Long): Option[Node] = { //TODO
    val observable = nodesCollection.find(equal("_id", id)).first()
    //FIXME observable.subscribe(observerLogger)
    val f = observable.toFuture()
    val nodes = Await.result(f, Duration.Inf).map(doc => doc.toJson().parseJson.convertTo[Node])
    nodes.headOption
  }

  override def getEdge(fromId: Long, toId: Long, edgeType: Level.Value): Option[Edge] = {
    None //TODO
  }

  override def updateNode(id: Long, content: Content): Boolean = {
    false //TODO
  }

  override def findNodesTo(startNodeId: Long): mutable.HashSet[Node] = {
    null //TODO
  }

  override def addEdge(from: Long, to: Long, cost: Long, edgeType: Level.Value): Edge = {
    null //TODO
  }

  override def removeNode(id: Long): Unit = {
    //TODO
  }

  override def findNodesFrom(startNodeId: Long): mutable.HashSet[Node] = {
    null //TODO
  }
}

package org.bitbucket.fabiopinheiro.service

import org.bitbucket.fabiopinheiro.{ Content, Edge, MetaData, Node }

import scala.collection.mutable._

object DB extends StorageInterface with MetaManager {
  var nodes = ArrayBuffer[Node]()
  var edges = HashSet[Edge]() //FROM TO
  var centroidEdges = HashSet[Edge]()
  var nextNodeID: Long = 1

  private def getEdgesSet(edgeType: Level.Value): HashSet[Edge] = {
    edgeType match {
      case Level.Base => edges
      case Level.Cache => centroidEdges
    }
  }

  override def addNode(content: Option[Content], nodeType: Level.Value): Node = synchronized {
    val metaData = nodeType match {
      case Level.Base => MetaData(0, 0, 0, 0, isCentroid = false)
      case Level.Cache => MetaData(nextNodeID, 0, nextNodeID, 0, isCentroid = true)
    }
    val node = Node(nextNodeID, content, metaData)
    nodes += node
    nextNodeID += 1 // scala that not have "++" operator ?
    node
  }

  override def getNode(id: Long): Option[Node] = {
    nodes.find(node => node.id == id)
  }

  override def updateNode(id: Long, content: Content): Boolean = synchronized {
    nodes.find(node => node.id == id) match {
      case Some(node: Node) => node.content = Some(content); true;
      case None => false;
    }
  }

  override def removeNode(id: Long) {
    synchronized {
      nodes = nodes.filterNot(node => node.id == id)
    }
  }

  override def addEdge(from: Long, to: Long, cost: Long = 1, edgeType: Level.Value = Level.Base): Edge = synchronized {
    val nodeFrom = getNode(from).get
    val nodeTo = getNode(to).get
    val newEdge = Edge(from, to, cost)
    if (!getEdgesSet(edgeType).add(newEdge)) {
      new RuntimeException
    }
    if (edgeType == Level.Base) updateMateData(newEdge)
    newEdge
  }

  override def getEdge(fromId: Long, toId: Long, nodeType: Level.Value): Option[Edge] = {
    val edgeSet = nodeType match {
      case Level.Base => edges
      case Level.Cache => centroidEdges
    }
    edgeSet.find(e => e.from == fromId && e.to == toId)
  }

  override def findNodesFrom(startNodeId: Long): HashSet[Node] = {
    edges.filter(e => e.from == startNodeId).map(e => getNode(e.to).get)
  }

  override def findNodesTo(startNodeId: Long): HashSet[Node] = {
    edges.filter(e => e.to == startNodeId).map(e => getNode(e.from).get)
  }
}

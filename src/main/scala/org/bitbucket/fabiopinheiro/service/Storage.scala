package org.bitbucket.fabiopinheiro.service

import org.bitbucket.fabiopinheiro.{ Content, Edge, MetaData, Node }

import scala.collection.mutable

object Level extends Enumeration { val Cache, Base = Value }

trait StorageInterface {

  def addNode(content: Option[Content], nodeType: Level.Value = Level.Base): Node
  def getNode(id: Long): Option[Node]
  def updateNode(id: Long, content: Content): Boolean
  def removeNode(id: Long)

  def getEdge(fromId: Long, toId: Long, edgeType: Level.Value = Level.Base): Option[Edge]
  def addEdge(from: Long, to: Long, cost: Long = 0, edgeType: Level.Value = Level.Base): Edge

  def findNodesFrom(startNodeId: Long): mutable.HashSet[Node]
  def findNodesTo(startNodeId: Long): mutable.HashSet[Node]
}

trait MetaManager extends StorageInterface {
  /**
   * This method update the metaData on the nodes has necessary after add a edge.
   *
   * @param edge the edge added to the graph
   */
  def updateMateData(edge: Edge): Unit = {
    //BUG FIXME updateCentroidEdges is just check a level depth! But i have a solution with a process of O(d) where d distance between centroids
    def updateCentroidEdges(from: Node, to: Node): Unit = { //call only after MetaData being updated
      def aux(fromCentroidId: Long, toCentroidId: Long, cost: Long): Unit = {
        if (fromCentroidId != 0 && toCentroidId != 0 && fromCentroidId != toCentroidId) {
          //Only in this case may be necessary to update the centroidEdge
          getEdge(fromCentroidId, toCentroidId, Level.Cache) match { //
            case Some(e) => if (e.cost > cost) { e.cost = cost } //the path already exists but is now shorter
            case None => addEdge(fromCentroidId, toCentroidId, cost, Level.Cache) //the path is added
          }
        }
      }
      var allNodesThanIGoingTo = findNodesFrom(to.id).map(n => (n.metaData.toMNodeId, n.metaData.toMNCost + 1))
      allNodesThanIGoingTo.add((to.metaData.toMNodeId, to.metaData.toMNCost))
      var allNodesThanIComingFrom = findNodesTo(from.id).map(n => (n.metaData.fromMNodeId, n.metaData.fromMNCost + 1))
      allNodesThanIComingFrom.add((from.metaData.fromMNodeId, from.metaData.fromMNCost))
      for (t <- allNodesThanIGoingTo; f <- allNodesThanIComingFrom) yield { aux(f._1, t._1, f._2 + t._2 + 1) }
    }

    def updateTail(tail: Node, head: Node): Unit = { //update tail.from with head.to // O(d/2)? where d distance between centroids
      val tailMeta = tail.metaData; val headMeta = head.metaData
      if (!tailMeta.isCentroid & headMeta.toMNodeId != 0 //is going to a Centroid!
        & (tailMeta.toMNodeId == 0 | tailMeta.toMNCost > headMeta.toMNCost + 1)) { //WOW another path and is cheaper. Man, let's go this way!
        tailMeta.toMNCost = headMeta.toMNCost + 1; tailMeta.toMNodeId = headMeta.toMNodeId
        findNodesTo(tail.id).foreach(n => updateTail(tail = n, head = tail))
      }
    }

    def updateHead(head: Node, tail: Node): Unit = { //update to the MetaData from O(d/2)
      val headMeta = head.metaData; val tailMeta = tail.metaData
      if (!headMeta.isCentroid & tailMeta.fromMNodeId != 0 //is coming from a Centroid!
        & (headMeta.fromMNodeId == 0 | headMeta.fromMNCost > tailMeta.fromMNCost + 1)) { //what is the shortest way back?
        headMeta.fromMNCost = tailMeta.fromMNCost + 1; headMeta.fromMNodeId = tailMeta.fromMNodeId
        findNodesFrom(head.id).foreach(n => updateHead(head = n, tail = head))
      }
    }

    val from = getNode(edge.from).get
    val to = getNode(edge.to).get
    updateTail(tail = from, head = to)
    updateHead(tail = from, head = to)
    updateCentroidEdges(from, to)
  }

}

package org.bitbucket.fabiopinheiro

import org.bitbucket.fabiopinheiro.client._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.util.{ Failure, Success }

object ClientMain extends App with SystemSettings with ClientFunction {
  def logInfo(msg: String, future: Future[Any]): Unit = {
    future.onComplete {
      case Success(e) => logger.info(s"#Success($msg): " + e.toString)
      case Failure(t) => logger.info(s"#Failure($msg): " + t.getMessage)
    }
  }
  def waitAndLog(msg: String, future: Future[Any]): Unit = {
    Await.ready(future, Duration.Inf)
    logInfo(msg, future)
  }

  logger.info("Hello, I'm the Client")

  val a = nodeCreate(Content(Some("Artur"), None))
  val e = nodeCreate(Content(Some("Esmeralda"), Some("group1")))
  val j = nodeCreate(Content(Some("Jose"), None))
  val m = nodeCreate(Content(Some("Marco"), Some("g test")))
  val seq = Seq(a, e, j, m)
  seq.foreach(f => Await.ready(f, Duration.Inf)) //Wait
  seq.foreach(f => logInfo("stage 1", f))

  waitAndLog("stage 2", nodeRead(1))
  waitAndLog("stage 3", nodeUpdate(1, Content(Some("João"), None)))
  waitAndLog("stage 4", nodeRead(1))
  waitAndLog("stage 5", nodeDelete(1))
  waitAndLog("stage 6", nodeRead(1))

  logger.info("Client: bye bye")

}
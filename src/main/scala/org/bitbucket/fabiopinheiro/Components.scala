package org.bitbucket.fabiopinheiro

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Node(id: Long, var content: Option[Content], metaData: MetaData)
case class Edge(from: Long, to: Long, var cost: Long)
case class Content(name: Option[String], group: Option[String])
case class MetaData(var fromMNodeId: Long, var fromMNCost: Long, var toMNodeId: Long, var toMNCost: Long, var isCentroid: Boolean)

object Node {
  def apply3(id: Long, content: Option[Content], metaData: MetaData): Node = Node(id, content, metaData)
  def apply(id: Long, content: Option[Content]): Node = Node(id, content, MetaData(id, 0, id, 0, false))
}
object Edge {
  def apply3(from: Long, to: Long, cost: Long): Edge = Edge(from, to, cost)
  def apply(from: Long, to: Long): Edge = Edge(from, to, 1)
}

//http://danielwestheide.com/blog/2012/12/19/the-neophytes-guide-to-scala-part-5-the-option-type.html

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val contentFormat = jsonFormat2(Content.apply)
  implicit val metaDataFormat = jsonFormat5(MetaData.apply)
  implicit val nodeFormat = jsonFormat3(Node.apply3)
  implicit val edgeFormat = jsonFormat3(Edge.apply3)
}

trait JsonSupportMongo extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val contentFormat = jsonFormat2(Content.apply)
  implicit val metaDataFormat = jsonFormat5(MetaData.apply)
  implicit val nodeFormat = { //HACK support the MongoDB json : ObjectId()
    val Array(p1, p2, p3) = extractFieldNames(scala.reflect.classTag[Node])
    val List(p1s, p2s, p3s) = Seq(p1, p2, p3).map { s => if (s == "id") "_id" else s } //FIXME HACK
    jsonFormat(Node.apply3, p1s, p2s, p3s)
  }
  implicit val edgeFormat = { //HACK support the MongoDB json : ObjectId()
    val Array(p1, p2, p3) = extractFieldNames(scala.reflect.classTag[Node])
    val List(p1s, p2s, p3s) = Seq(p1, p2, p3).map { s => if (s == "id") "_id" else s } //FIXME HACK
    jsonFormat3(Edge.apply3)
  }
}


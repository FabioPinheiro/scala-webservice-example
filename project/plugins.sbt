resolvers += Classpaths.sbtPluginReleases

//sbt-revolver which is helpful for development. It recompiles and runs our microservice every time the code in files changes. Notice that it is initialized inside build.sbt.
//addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")

//sbt-assembly: is a great library that lets us deploy our microservice as a single .jar file.

//addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.3")

//sbt-native-packager: is needed by Heroku to stage the app.
//addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.0-RC1")

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.6.0") // support for source code formatting using Scalariform

// /sbt-scapegoat is a plugin for SBT that integrates the scapegoat static code analysis library. Find out more about scapegoat at the scapegoat project page.
addSbtPlugin("com.sksamuel.scapegoat" %% "sbt-scapegoat" % "1.0.4")

//sbt scalastyleGenerateConfig     command to get the configuration file "scalastyle-config.xml "
//sbt scalastyle                   command to check the code with the scalastyle command
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.8.0")

//sbt-scoverage is a plugin for SBT that integrates the scoverage code coverage library
//addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.3.5")

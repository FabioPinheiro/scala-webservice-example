Scala-WebService-Example
=========================
This project is based on the draft of the projects from:

https://github.com/yeghishe/minimal-scala-akka-http-seed
Licensed under the Apache License, Version 2.0

https://github.com/owainlewis/activator-akka-http
Licensed under the Apache License, Version 2.0

http://doc.akka.io/docs/akka/snapshot/java/futures.html
http://doc.akka.io/docs/akka/snapshot/scala/actors.html
https://jaxenter.com/tutorial-asynchronous-programming-with-akka-actors-105682.html

=========================
# Compile
sbt compile
// At the moment que Server and the Cliente are compiled together

# How to run
sbt "run-main org.bitbucket.fabiopinheiro.ServerMain"
sbt "run-main org.bitbucket.fabiopinheiro.ClientMain"
sbt "run-main org.bitbucket.fabiopinheiro.ShortestPath"
sbt "run-main org.bitbucket.fabiopinheiro.MongoMain"

# Deploying
//When deploying the projecto as a .jar file, one can overwrite the configuration values when running the jar.
java -jar projecto.jar -Dservices.ip-api.port=8080


## Test the connectivity with the server using the curl tool

curl -X POST -H 'Content-Type: application/json' localhost:9000/node -d '{"name": "Maria"}';  echo;

curl http://localhost:9000/node?id=1; echo;

curl -X DELETE http://localhost:9000/node?id=1; echo

curl -X POST -H 'Content-Type: application/json' localhost:9000/node \
-d '{"name": "Sandr", "group": "group 2"}'; echo;

curl -X PUT -H "Content-Type: application/json" localhost:9000/node?id=2 \
-d '{"name": "Sandra", "group": "2"}'; echo;

curl http://localhost:9000/node?id=2; echo;

curl -X POST -H 'Content-Type: application/json' localhost:9000/edge \
-d '{"from": 1, "to": 2}'; echo;






